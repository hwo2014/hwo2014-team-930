import json
import socket
import sys

from piece_graph import generate_graph
from car import Car


class AwesomeBot(object):
    def __init__(self, socket, name, key, map_name=''):
        self.socket = socket
        self.name = name
        self.key = key
        self.last_position = None
        self.last_time = None
        self.velocity = 0
        self.car = None
        self.map_name = map_name
        self.tick = None

    def msg(self, msg_type, data):
        msg = {"msgType": msg_type, "data": data}
        if self.tick:
            msg["gameTick"] = self.tick
        self.send(json.dumps(msg))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        if self.map_name:
            return self.msg(
                "joinRace",
                {
                    "botId": {
                        "name": self.name,
                        "key": self.key
                    },
                    "trackName": self.map_name,
                    "carCount": 1
                }
            )
        else:
            return self.msg(
                "join",
                {
                    "name": self.name,
                    "key": self.key
                }
            )

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, msg):
        data = msg['data']
        print("Joined: {}".format(data))
        self.ping()

    def on_game_start(self, msg):
        data = msg['data']
        print("Race started: {}".format(data))
        self.ping()

    def calculate_velocity(self, our_car, msg):
        if self.last_position and self.last_time:
            last_piece_position = self.last_position[u'piecePosition']
            current_piece_position = our_car[u'piecePosition']
            if last_piece_position[u'pieceIndex'] == \
               current_piece_position[u'pieceIndex'] and \
               self.tick != self.last_time:
                self.velocity = (current_piece_position[u'inPieceDistance']
                            - last_piece_position[u'inPieceDistance']) / (
                               self.tick - self.last_time )
        self.last_time = self.tick
        self.last_position = our_car

    def on_car_positions(self, msg):
        data = msg['data']
        our_car = None
        for car in data:
            if car['id'] == self.car.car_id:
                our_car = car
        if our_car is None:
            print("Error, our car wasn't found!")
            return
        piece_position = our_car[u'piecePosition']
        piece_index = piece_position[u'pieceIndex']
        lane = piece_position['lane']['endLaneIndex']
        lap = piece_position['lap']
        optimal_path, distance = self.piece_graph.shortest_path(piece_index, lane)
        self.calculate_velocity(our_car, msg)
        if self.car.last_switch != piece_index and len(optimal_path) > 2:
            next_node = optimal_path[0]
            target_node = optimal_path[1]
            if target_node.lane != lane and next_node.switch:
                self.car.last_switch = piece_index
                if target_node.lane < lane:
                    self.msg("switchLane", "Left")
                else:
                    self.msg("switchLane", "Right")
                return
        self.throttle(.6)


    def on_crash(self, msg):
        data = msg['data']
        print("Someone crashed: {}".format(data))
        self.ping()

    def on_game_end(self, msg):
        data = msg['data']
        print("Race ended: {}".format(data))
        self.ping()

    def on_error(self, msg):
        data = msg['data']
        print("Error: {0}".format(data))
        self.ping()

    def on_your_car(self, msg):
        self.car = Car(msg['data'])

    def on_game_init(self, msg):
        data = msg['data']
        race = data['race']
        track = race['track']
        pieces = track['pieces']
        self.pieces = pieces
        self.piece_graph = generate_graph(race)

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'yourCar': self.on_your_car,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            if 'gameTick' in msg:
                self.tick = msg['gameTick']
            msg_type = msg['msgType']
            if msg_type in msg_map:
                msg_map[msg_type](msg)
            else:
                print("Got {0}".format(msg))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) < 5:
        print("Usage: ./run host port botname botkey [mapname]")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        map_name = ''
        if len(sys.argv) == 6:
            map_name = sys.argv[5]
        bot = AwesomeBot(s, name, key, map_name)
        bot.run()
