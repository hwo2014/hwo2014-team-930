from math import pi, copysign
from heapq import heappush, heappop

class _PieceNode(object):
    def __init__(self, lane, distance, piece_index, children, switch):
        self.lane = lane
        self.distance = distance
        self.piece_index = piece_index
        self.children = children
        self.switch = switch

    def __repr__(self):
        return "node piece_index: {} lane: {} distance: {}".format(
            self.piece_index, self.lane, self.distance)

    def print_graph_bfs(self):
        def print_node(level, node):
            for a in range(0, level):
                print(" "),
            print(node)
        def push_children(child_level, node):
            for child in node.children:
                queue.append((child_level, child))
        queue = [(0, self)]
        while queue:
            (level, node), queue = queue[0], queue[1:]
            print_node(level, node)
            push_children(level + 1, node)


class PieceGraph(object):
    def __init__(self, graph, total_lanes, total_laps):
        self.graph = graph
        self.total_lanes = total_lanes
        self.total_laps = total_laps

    def shortest_path(self, piece_index, lane):
        def push_child(child, child_lap):
            new_node = ((child.distance + total_distance), child_lap, child, path +
                        [child])
            heappush(nodes, new_node)
        root = self.graph[piece_index][lane]
        nodes = [(0, 0, root, [])]
        while nodes:
            total_distance, current_lap, current, path = heappop(nodes)
            if current.switch:
                # if this is a switch, we don't care about the other nodes with
                # a piece index less than this, just empty the traversal
                nodes = [node for node in nodes
                         if (node[2].piece_index > current.piece_index or node[1] > current_lap)]
            if current.children:
                for child in current.children:
                    push_child(child, current_lap)
            #predict 1 lap ahead
            elif current_lap + 1 < 2:
                # start the next lap
                if current.switch:
                    if lane > 0:
                        push_child(self.graph[0][lane-1], current_lap + 1)
                    if lane < self.total_lanes:
                        push_child(self.graph[0][lane+1], current_lap + 1)
                push_child(self.graph[0][lane], current_lap + 1)
            else:
                return path, total_distance

def generate_graph(race):
    def depth_first_search(current_lane, current_pieces, piece_index):
        try:
            return graph[piece_index][current_lane]
        except KeyError:
            pass
        current, next_section = current_pieces[0], current_pieces[1:]
        distance = 0
        if 'radius' in current:
            angle_sign = -copysign(1, current['angle'])
            actual_radius = abs(angle_sign * current['radius']
                                + lanes[current_lane])
            distance = pi * (actual_radius * 2) * abs(
                current['angle']) / 360.0
        else:
            distance = current['length']
        children = []
        switch = False
        if next_section:
            next_piece_index = (piece_index + 1) % max_piece_index
            if 'switch' in current and current['switch']:
                switch = True
                if current_lane > 0:
                    children.append(depth_first_search(current_lane - 1,
                                                       next_section, next_piece_index))
                if current_lane < max_lane:
                    children.append(depth_first_search(current_lane + 1, next_section,
                                                       next_piece_index))
            children.append(depth_first_search(current_lane, next_section,
                                               next_piece_index))
        ret = _PieceNode(current_lane, distance, piece_index, children, switch)
        piece_section = graph.setdefault(piece_index, {})
        piece_section[current_lane] = ret
        return ret

    track = race['track']
    lanes = { lane['index']: lane['distanceFromCenter'] for lane in track['lanes'] }
    max_lane = max(lanes.keys())
    children = None
    pieces = track['pieces']
    max_piece_index = len(pieces)
    graph = {}
    if pieces:
        for lane in lanes.keys():
            depth_first_search(lane, pieces, 0)

    try:
        session = race['raceSession']
        laps = session['laps']
    except KeyError:
        laps = 1
    piece_graph = PieceGraph(graph, max_lane, laps)
    return piece_graph
